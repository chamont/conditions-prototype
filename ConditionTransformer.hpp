#ifndef CONDITION_TRANSFORMER_HPP
#define CONDITION_TRANSFORMER_HPP

#include <array>
#include <functional>
#include <tuple>

#include "cpp_next/utility.hpp"

#include "ConditionAlgBase.hpp"
#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionSlot.hpp"

#include "detail/identifiers.hpp"


// Forward declaration of other prototype components
class ConditionSvc;


// ConditionTransformer is an adaptation of the Gaudi::Functional::Transformer concept to condition derivation.
// It helps writing condition derivation algorithms by automating the tedious work of fetching inputs, unpacking
// their value from the ConditionData, computing the IoV of the output, and writing the output down.
//
// Since this class assumes that algorithms are only used to produce derived conditions, it is NOT
// available when IO inside of ConditionAlgs is enabled.
//
#ifndef ALLOW_IO_IN_ALGORITHMS


// Like Gaudi::Functional::Transformer, ConditionTransformer is templated by a function signature.
// Since the C++ template system is broken beyond repair, we need to implement this through specialization.
template< typename >
class ConditionTransformer;


// ConditionTransformer acts as a base class to a concrete user-defined functor, which defines an operator()
// with the function signature given as a template parameter.
template< typename Result,
          typename... Args >
class ConditionTransformer< Result(Args...) > : public ConditionAlgBase
{
public:

   // ConditionTransformer can be scheduled by Gaudi as a ConditionAlg
   void execute( const ConditionSlot & slot ) const final override;


protected:

   // To initialize a ConditionTransformer, a user must provide it with the location of the ConditionSvc
   // and specify the condition identifiers of its result and arguments.
   using ConditionIDRef = std::reference_wrapper< const detail::ConditionID >;
   using ArgsIDs = std::array< ConditionIDRef, sizeof...(Args) >;
   ConditionTransformer(       ConditionSvc        &  conditionService,
                               detail::Scheduler   &  scheduler,
                         const detail::ConditionID &  resultID,
                               ArgsIDs             && argsIDs );

   // The user then implements the condition derivation logic, and ConditionTransformer takes care of the rest
   virtual Result operator()( const Args & ... args ) const = 0;


private:

   // We need a layer of indirection below the constructor in order to generate the index sequence that will be
   // used to register the algorithm's condition inputs
   using ArgsHandles = std::tuple< std::reference_wrapper< const ConditionReadHandle<Args> >... >;
   template< std::size_t... ArgIndexes >
   ArgsHandles registerArgs( ArgsIDs && argsIDs,
                             cpp_next::index_sequence<ArgIndexes...> );

   // We need a layer of indirection below execute() in order to generate the index sequence that will be used
   // to access the algorithm's condition inputs.
   template< std::size_t... ArgIndexes >
   void executeImpl( const ConditionSlot & slot,
                     cpp_next::index_sequence<ArgIndexes...> ) const;

   // Derive the output condition from the input arguments that we received
   ConditionData< Result > deriveOutput( const ConditionData<Args> & ... args ) const;

   // The input and output handles are stored here
   const ConditionWriteHandle<Result> & resultHandle;
   const ArgsHandles argsHandles;

};


// Since ConditionTransformer is intended for condition derivation, algorithms without inputs are not allowed.
template< typename Result >
class ConditionTransformer< Result() >;


// Inline implementations
#include "ConditionTransformer.ipp"


#endif

#endif
