#ifndef CONDITION_SLOT_HPP
#define CONDITION_SLOT_HPP

#include <functional>

#include "cpp_next/future.hpp"
#include "cpp_next/optional.hpp"

#include "detail/ConditionSlotID.hpp"


// Forward declaration of other prototype components
class TransientConditionStorageSvc;


// This class is an RAII wrapper around a condition storage slot. It takes care of liberating it after event processing.
class ConditionSlot
{
public:

   // === BASIC WRAPPER OPERATION ===

   // ConditionSlots wrap an implementation-defined identifier of condition storage
   const detail::ConditionSlotID & id() const;

   // Every time an event is allocated a condition slot, the TransientConditionStorageSvc creates such a wrapper
   ConditionSlot( detail::ConditionSlotID      && id,
                  TransientConditionStorageSvc &  host );

   // The wrapper is movable but not copyable: the intended use is to put in the EventContext and keep it there
   ConditionSlot( const ConditionSlot & ) = delete;
   ConditionSlot( ConditionSlot && other );
   ConditionSlot & operator=( const ConditionSlot & ) = delete;
   ConditionSlot & operator=( ConditionSlot && other );

   // On destruction, the wrapper will make sure that the condition slot is liberated
   ~ConditionSlot();


   // === ADVANCED FEATURES ===

   // During condition slot initialization, before the condition slot has reached its final location, one
   // may need non-owning references to it, for example to carry out asynchronous IO.
   //
   // In this case, it is not safe to use a const ConditionSlot &, because that reference may be invalidated
   // if the ConditionSlot wrapper is moved. The following mechanism should be used instead.
   //
   ConditionSlot ref() const;

   // One can also check if two events use the same condition storage slot
   bool operator==( const ConditionSlot & other ) const;
   bool operator!=( const ConditionSlot & other ) const;


private:

   cpp_next::optional< detail::ConditionSlotID > m_id;
   std::reference_wrapper< TransientConditionStorageSvc > m_host;
   bool m_isOwner;

   // Non-owning reference constructor
   ConditionSlot( const detail::ConditionSlotID & m_id,
                  TransientConditionStorageSvc  & host );
   
};


// To hide the latency of slot alocation and filling, futures of condition slots will be used in various places
using ConditionSlotFuture = cpp_next::future< ConditionSlot >;


// Inline implementations
#include "ConditionSlot.ipp"

#endif
