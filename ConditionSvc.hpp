#ifndef CONDITION_SVC_HPP
#define CONDITION_SVC_HPP

#include <functional>
#include <vector>

#include "ConditionSlot.hpp"
#include "exceptions.hpp"

#include "detail/IConditionIOSvc.hpp"
#include "detail/timing.hpp"


// Forward declaration of other prototype components
class TransientConditionStorageSvc;


// Centralized entry point for condition infrastructure coordination and interactions with the Gaudi Scheduler.
class ConditionSvc
{
public:

   // To construct the ConditionSvc, we need a place to store conditions in RAM
   ConditionSvc( TransientConditionStorageSvc & transientStore );

   // The condition service is neither movable nor copyable
   ConditionSvc( const ConditionSvc & ) = delete;
   ConditionSvc( ConditionSvc && ) = delete;
   ConditionSvc & operator=( const ConditionSvc & ) = delete;
   ConditionSvc & operator=( ConditionSvc && ) = delete;

   // Access condition storage
   TransientConditionStorageSvc & transientStore() const;

   // Register a condition IO service
   void registerConditionProducer( detail::IConditionIOSvc & ioService );
   class ConditionProducerAlreadyRegistered : public ConditionPrototypeException { };

   // Setup the conditions infrastructure (allocate a storage slot, perform IO...) for a new event.
   // Returns a future that will eventually provide this event's condition slot.
   ConditionSlotFuture setupConditions( const detail::TimePoint & eventTimestamp );


private:

   TransientConditionStorageSvc & m_transientStore;
   std::vector< std::reference_wrapper<detail::IConditionIOSvc> > m_ioServices;

};

#endif
