#include <algorithm>

#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/ConditionSlotIDFutures.hpp"


TransientConditionStorageSvc::TransientConditionStorageSvc( const std::size_t    capacity )
   : m_storage( std::max(capacity, 1ul) )
{ }


std::size_t
TransientConditionStorageSvc::max_capacity()
{
   return UNBOUNDED_STORAGE;
}


std::size_t
TransientConditionStorageSvc::availableStorage()
{
   return m_storage.availableSlots();
}


ConditionSlotFuture
TransientConditionStorageSvc::allocateSlot( const detail::TimePoint & eventTimestamp )
{
   // Wrap the condition slot identifier from the ConditionStore into an RAII wrapper
   return m_storage.allocateSlot( eventTimestamp ).then(
      [this]( detail::ConditionSlotIDFuture && idFuture ) -> ConditionSlot {
         detail::ConditionSlotID id = idFuture.get();
         return ConditionSlot{ std::move(id), *this };
      }
   );
}


void
TransientConditionStorageSvc::liberateSlot( const detail::ConditionSlotID slotID )
{
   return m_storage.liberateSlot( slotID );
}


detail::ConditionDataflow::Metadata
TransientConditionStorageSvc::getConditionDataflow() const
{
   return m_dataflow.report();
}
