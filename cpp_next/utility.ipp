namespace cpp_next
{

// Implementation of C++14's make_index_sequence
namespace make_index_sequence_impl
{

// We use a binary merge algorithm in order to bound the amount of template instantiations to log2(N).
// In addition, since the sizeof... operator is implemented quite inefficiently in some compilers, it is
// best if the user manually passes in the size of the first sequence as a template parameter.
template< typename Sequence1,
          typename Sequence2,
          std::size_t lengthOfSequence1 >
struct MergedSequence;


// The sequence merging algorithm turns two sequences 0 .. N-1 and 0 .. M-1 into the sequence 0 .. N+M-1
template< std::size_t... seq1,
          std::size_t... seq2,
          std::size_t lengthOfSeq1 >
struct MergedSequence<
   index_sequence< seq1... >,
   index_sequence< seq2... >,
   lengthOfSeq1
> : index_sequence< seq1..., (lengthOfSeq1 + seq2)... > { };


// With such a sequence merging algorithm, we can implement divide-and-conquer integer sequence generation.
// The longer half of the sequence should go first, as the items of the second half will all need to
// be incremented by the length of the first half.
template< std::size_t length >
struct GeneratedSequence : public MergedSequence< typename GeneratedSequence< length - (length/2) >::type,
                                                  typename GeneratedSequence< length/2 >::type,
                                                  length - (length/2) > { };


// This recursive algorithm needs two endpoints: one for null sequences, and one for singleton sequences
template<> struct GeneratedSequence< 0 > : index_sequence<> { };
template<> struct GeneratedSequence< 1 > : index_sequence< 0 > { };

}

}
