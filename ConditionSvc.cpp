#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/single_use_bind.hpp"


ConditionSvc::ConditionSvc( TransientConditionStorageSvc & transientStore )
   : m_transientStore( transientStore )
{ }


TransientConditionStorageSvc &
ConditionSvc::transientStore() const
{
   return m_transientStore;
}


void
ConditionSvc::registerConditionProducer( detail::IConditionIOSvc & ioService )
{
   // Check that the condition producer is not already registered
   bool already_registered = std::any_of(
      m_ioServices.cbegin(),
      m_ioServices.cend(),
      [&ioService]( const std::reference_wrapper<detail::IConditionIOSvc> & candidate ) -> bool {
         return ( &(candidate.get()) == &ioService );
      }
   );
   if( already_registered ) throw ConditionProducerAlreadyRegistered{};
   
   // Register new condition producer
   m_ioServices.emplace_back( ioService );
}


ConditionSlotFuture
ConditionSvc::setupConditions( const detail::TimePoint & eventTimestamp )
{
   // Schedule allocation of a condition slot, followed by condition IO
   return ConditionSlotFuture{
      m_transientStore.allocateSlot( eventTimestamp ).then(
         [this, eventTimestamp]( ConditionSlotFuture && slotFuture ) -> ConditionSlotFuture
         {
            // Fetch the condition slot from the input future
            ConditionSlot slot = slotFuture.get();
            
            // Set up storage for the condition IO futures
            std::vector< cpp_next::future<void> > ioFutures;
            ioFutures.reserve( m_ioServices.size() );
            
            // Start all condition IO
            for( const auto & service : m_ioServices ) {
               ioFutures.emplace_back( service.get().startConditionIO( eventTimestamp, slot ) );
            }
            
            // Prepare to manipulate the union of these IO futures, which represents IO completion
            using IOCompletionFuture = decltype( cpp_next::when_all( ioFutures.begin(), ioFutures.end() ) );

            // After the IO is complete, we will check if it went well and move the condition slot down the chain
            auto ioContinuationImpl =
               []( ConditionSlot && slot, IOCompletionFuture && ioFuture ) -> ConditionSlot {
                  ioFuture.get();
                  return std::move(slot);
               };

            // That continuation hence needs to take ownership of the condition slot.
            // In C++14, we'll do this directly in the lambda by using move capture and a mutable operator().
            auto ioContinuation = detail::single_use_bind(
               std::move(ioContinuationImpl),
               std::move(slot),
               std::placeholders::_1
            );
            
            // Schedule the IO continuation
            return cpp_next::when_all( ioFutures.begin(), ioFutures.end() ).then(
               std::move(ioContinuation)
            );
         }
      )
   };
}
