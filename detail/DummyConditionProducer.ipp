namespace detail
{

template< typename T >
DummyConditionProducer::DummyConditionProducer( ConditionWriteHandle<T>     && writeHandle,
                                                detail::ConditionDataSet<T> && mockDataSet )
   : m_impl{ new Implementation<T>{ std::move(writeHandle), std::move(mockDataSet) } }
{ }


template< typename T >
DummyConditionProducer::Implementation<T>::Implementation( ConditionWriteHandle<T>     && writeHandle,
                                                           detail::ConditionDataSet<T> && mockDataSet )
   : m_handle{ std::move(writeHandle) }
   , m_dataSet{ std::move(mockDataSet) }
{ }


template< typename T >
bool
DummyConditionProducer::Implementation<T>::outputMissing( const ConditionSlot & targetSlot ) const
{
   return !m_handle.hasValue( targetSlot );
}


template< typename T >
void
DummyConditionProducer::Implementation<T>::produceOutput( const detail::TimePoint & eventTimestamp,
                                                          const ConditionSlot     & targetSlot ) const
{
   // Access the condition data from the mock dataset
   const auto & conditionData = m_dataSet.find( eventTimestamp );
   
   // Put it into transient storage
   m_handle.put( targetSlot, conditionData );
}

}
