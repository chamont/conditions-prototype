#include "exceptions.hpp"


namespace detail
{

inline
bool
AnyConditionHandleImpl::hasValue( const ConditionSlot & slot ) const
{
   return m_holder->hasValue( slot );
}


template< typename ConditionHandleType, typename Enable >
AnyConditionHandleImpl::AnyConditionHandleImpl( ConditionHandleType && handle )
   : m_holder{ new ConditionHandleType{ std::move(handle) } }
{ }


template< typename ConditionHandleType, typename Enable >
const ConditionHandleType &
AnyConditionHandleImpl::get() const
{
   // Extract a raw pointer-to-base from our internal holder
   const ConditionHandleBase * basePtr = m_holder.get();
   
   // Try to cast it into a pointer to the desired handle type
   const auto * derivedPtr = dynamic_cast< const ConditionHandleType * >( basePtr );

   // If a null pointer is returned, the user called the wrong version of get()
   if( !derivedPtr ) throw PrototypeBugDetected{};
   
   // Otherwise, we can return the result as a const-reference
   return *derivedPtr;
}

}
