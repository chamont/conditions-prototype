#include "ConditionUsage.hpp"
#include "exceptions.hpp"


namespace detail
{

void
ConditionUsage::setProducer( const detail::ConditionUserID & producerID )
{
   if( m_producer ) throw ProducerAlreadyRegistered{};
   m_producer = producerID;
}


cpp_next::optional< detail::ConditionUserID >
ConditionUsage::producer() const
{
   return m_producer;
}


void
ConditionUsage::addConsumer( const detail::ConditionUserID & consumerID )
{
   m_consumers.insert( consumerID );
}


detail::ConditionUserIDSet
ConditionUsage::consumers() const
{
   return m_consumers;
}

}
