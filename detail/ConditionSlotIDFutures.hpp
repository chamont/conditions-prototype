#ifndef CONDITION_SLOT_ID_FUTURES_HPP
#define CONDITION_SLOT_ID_FUTURES_HPP

#include <vector>

#include "cpp_next/future.hpp"

#include "detail/ConditionSlotID.hpp"


namespace detail
{

// To allow for asynchronous slot allocation, the ConditionStore outputs futures of condition slot IDs
using ConditionSlotIDFuture = cpp_next::future< ConditionSlotID >;

// To asynchronously signal these futures, it needs promises
using ConditionSlotIDPromise = cpp_next::promise< ConditionSlotID >;

// As the continuations to these futures might call ConditionStore methods, we need to make sure that
// the ConditionStore is in a consistent state before signalling these promises.
// The following helper class will be used to transmit future signaling homework to the appropriate scope.
class ReadySlotIDPromise
{
public:

   // Prepare the slot promise
   ReadySlotIDPromise(       ConditionSlotIDPromise && promise,
                       const ConditionSlotID           id );

   // Signal it once the ConditionStore back to a consistent state
   void process();

private:

   ConditionSlotIDPromise promise;
   ConditionSlotID id;

};

// We will repeatedly manipulate lists of such ready promises, better have a shorthand notation for it
using ReadySlotIDPromiseList = std::vector< ReadySlotIDPromise >;

}


// Inline implementations
#include "detail/ConditionSlotIDFutures.ipp"

#endif
