namespace detail
{

inline
ReadySlotIDPromise::ReadySlotIDPromise(       ConditionSlotIDPromise && p_promise,
                                        const ConditionSlotID           p_id )
   : promise{ std::move(p_promise) }
   , id{ p_id }
{ }


inline
void
ReadySlotIDPromise::process()
{
   promise.set_value( id );
}

}
