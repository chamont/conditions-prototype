#include <limits>

#include "timing.hpp"


namespace detail
{

const TimeInterval TimeInterval::NULL_INTERVAL{ 1, 0 };

const TimeInterval TimeInterval::INFINITE_INTERVAL{ std::numeric_limits<TimePoint>::min(),
                                                    std::numeric_limits<TimePoint>::max() };

}
