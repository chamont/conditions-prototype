#include <algorithm>

#include "exceptions.hpp"

#include "detail/ConditionSlotKnowledge.hpp"


namespace detail
{

ConditionSlotKnowledge::ConditionSlotKnowledge( const size_t capacity )
   : m_capacity{ capacity }                              // Remember how many slots we have around
   , m_slotUsage( capacity, 0 )                          // All slots are free for use
   , m_slotIoV( capacity, TimeInterval::NULL_INTERVAL )  // All slots must be filled with data before use
   , m_missingConditions( capacity, 0 )                  // No slot is being constructed
{ }


VersionedCondition &
ConditionSlotKnowledge::registerCondition( const ConditionID           & id,
                                           const OptionalConditionKind & kind )
{
   // Make sure that this condition has associated storage
   auto emplaceResult = m_data.emplace( id, VersionedCondition{ kind, m_capacity } );
   auto & versionedCondition = emplaceResult.first->second;
   
   // Lazily set the condition kind as soon as it becomes available (when the writer is registered)
   if( kind ) versionedCondition.kind = kind;
   
   // Give the caller access to the versioned condition data. The reason we can share a reference here
   // is that unordered_map insertion does not invalidate previously acquired references to data.
   return versionedCondition;
}


bool
ConditionSlotKnowledge::reserveSuitableSlot( const TimePoint       & eventTimestamp,
                                                   ConditionSlotID & slotID )
{
   return reserveSlotIf(
      [this, &eventTimestamp]( ConditionSlotID id ) -> bool {
         return m_slotIoV[id].contains( eventTimestamp );
      },
      slotID
   );
}


bool
ConditionSlotKnowledge::reserveUnusedSlot( ConditionSlotID & slotID )
{
   return reserveSlotIf(
      [this]( ConditionSlotID id ) -> bool {
         return ( m_slotUsage[id] == 0 );
      },
      slotID
   );
}


void
ConditionSlotKnowledge::reserveSlot( const ConditionSlotID slotID )
{
   ++m_slotUsage[slotID];
}


bool
ConditionSlotKnowledge::releaseSlot( const ConditionSlotID slotID )
{
   return ( --m_slotUsage[slotID] == 0 );
}


void
ConditionSlotKnowledge::setupSlot( const ConditionSlotID   slotID,
                                   const TimePoint       & eventTimestamp )
{
   // Initialize raw condition tracking
   m_missingRawConditions = 0;
   m_incomingSlotIoV = TimeInterval::INFINITE_INTERVAL;

   // Initialize missing condition tracking
   m_missingConditions[slotID] = 0;
   
   // Mark the slot as invalid until all raw conditions have been inserted
   m_slotIoV[slotID] = TimeInterval::NULL_INTERVAL;

   // For each condition that we manage...
   for( auto & mapItem : m_data )
   {
      // ...access the versioned dataset
      auto & versionedCondition = mapItem.second;
      
      // Look for a condition version that is suitable for the current event
      const auto & suitableVersionIterator = std::find_if(
         versionedCondition.data.cbegin(),
         versionedCondition.data.cend(),
         [&eventTimestamp]( const SharedConditionData & candidate ) -> bool {
            return candidate.validFor( eventTimestamp );
         }
      );
      
      // Set up the new condition slot according to our findings
      if( suitableVersionIterator != versionedCondition.data.end() )
      {
         const auto & suitableConditionData = *suitableVersionIterator;
         m_incomingSlotIoV.intersectWith( suitableConditionData.iov() );
         versionedCondition.data[slotID] = suitableConditionData;
      }
      else
      {
         ++m_missingConditions[slotID];
         if( versionedCondition.kind == ConditionKind::RAW ) {
            ++m_missingRawConditions;
         }
         versionedCondition.data[slotID].clear();
      }
   }
   
   // There should be missing raw conditions here. Otherwise, we would not
   // need to create a new slot, but could be reusing an existing one instead.
   if( m_missingRawConditions == 0 ) throw PrototypeBugDetected{};
}


ConditionSlotKnowledge::SlotCompletionEvents
ConditionSlotKnowledge::notifyConditionInsertion( const ConditionSlotID   slotID,
                                                  const ConditionKind     kind,
                                                  const TimeInterval    & iov )
{
   SlotCompletionEvents outcome = { false, false };

   // Keep track of the fact that a new condition was inserted
   if( --m_missingConditions[slotID] == 0 ) {
      outcome.slotFullyConstructed = true;
   }

   // If this was a raw condition, we also need to update the IoV metadata accordingly
   if( kind == ConditionKind::RAW )
   {
      m_incomingSlotIoV.intersectWith( iov );
      if( --m_missingRawConditions == 0 ) {
         m_slotIoV[slotID] = m_incomingSlotIoV;
         outcome.slotValidityAvailable = true;
      }
   }
   
   return outcome;
}


size_t
ConditionSlotKnowledge::availableSlots() const
{
   return std::count_if(
      m_slotUsage.cbegin(),
      m_slotUsage.cend(),
      []( const int referenceCount ) -> bool {
         return ( referenceCount == 0 );
      }
   );
}


const TimeInterval &
ConditionSlotKnowledge::slotIoV( const ConditionSlotID slotID ) const
{
   return m_slotIoV[slotID];
}


bool
ConditionSlotKnowledge::rawConditionsMissing() const
{
   return ( m_missingRawConditions != 0 );
}


bool
ConditionSlotKnowledge::slotFullyConstructed( const ConditionSlotID slotID ) const
{
   return ( m_missingConditions[slotID] == 0 );
}


bool
ConditionSlotKnowledge::reserveSlotIf( const SlotPredicate   & criterion,
                                             ConditionSlotID & slotID )
{
   // Try to reserve a slot that matches the provided criterion
   for( ConditionSlotID id = 0; id < m_capacity; ++id )
   {
      if( criterion(id) ) {
         reserveSlot( id );
         slotID = id;
         return true;
      }
   }
   
   // If control reaches this point, no suitable slot has been found
   return false;
}

}
