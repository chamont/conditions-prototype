#include <unordered_set>

#include "ConditionSvc.hpp"

#include "detail/Scheduler.hpp"


namespace detail
{

Scheduler::Scheduler( ConditionSvc & conditionService )
   : m_conditionService( conditionService )
{ }


void
Scheduler::registerConditionAlg( const IConditionAlg & alg )
{
   m_conditionAlgs.emplace_back( &alg );
}


void
Scheduler::simulateEventLoop( const std::vector< detail::TimePoint > & eventTimestamps )
{
   // Simulate a serial event loop
   for( const auto & timestamp : eventTimestamps )
   {
      // Set up a condition slot, carrying out condition IO as necessary
      ConditionSlotFuture slotFuture = m_conditionService.setupConditions( timestamp );
      const ConditionSlot slot = slotFuture.get();

      // Determine which condition algorithms need to run
      using ConditionAlgSet = std::unordered_set< ConditionAlgPtr >;
      ConditionAlgSet scheduledAlgs;
      std::copy_if(
         m_conditionAlgs.cbegin(),
         m_conditionAlgs.cend(),
         std::inserter( scheduledAlgs, scheduledAlgs.end() ),
         [&slot]( const ConditionAlgPtr alg ) -> bool {
            return alg->outputMissing( slot );
         }
      );

      // Brute-force the algorithm dependency graph until all scheduled algorithms have run
      while( !scheduledAlgs.empty() )
      {
         // We need a way to detect scheduler stall caused by circular dependencies.
         // These translate into no ConditionAlg managing to run although Algs still remain.
         bool stallDetected = true;
         
         // Try to run every scheduled algorithm
         for( auto algIt = scheduledAlgs.begin(); algIt != scheduledAlgs.end(); ) 
         {
            const auto & alg = **algIt;
            if( alg.inputsPresent( slot ) )
            {
               // This algorithm is ready to run, so run it and remove it from the scheduled set
               alg.execute(
                  slot
#ifdef ALLOW_IO_IN_ALGORITHMS
                , timestamp
#endif
               );
               algIt = scheduledAlgs.erase( algIt );

               // If we managed to run at least one algorithm, no stall occured
               stallDetected = false;
            }
            else
            {
               // We cannot run this algorithm, skip it for now
               ++algIt;
            }
         }

         // If a scheduler stall was detected, we must report the bad news
         if( stallDetected ) throw SchedulerStallDetected{};
      }
   }
}

}
