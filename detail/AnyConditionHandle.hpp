#ifndef ANY_CONDITION_HANDLE_HPP
#define ANY_CONDITION_HANDLE_HPP

#include "detail/AnyConditionHandleImpl.hpp"


namespace detail
{

// Type-erased holder of ConditionReadHandles
class AnyConditionReadHandle
{
public:
   
   // Encapsulate a concrete condition read handle into the holder
   template< typename T >
   AnyConditionReadHandle( ConditionReadHandle<T> && handle );

   // Get read-only access to the stored handle. PrototypeBugDetected will be thrown if the type is wrong.
   template< typename T >
   const ConditionReadHandle<T> & get() const;

   // Access the hasValue() method of the stored handle
   bool hasValue( const ConditionSlot & slot ) const;


private:

   AnyConditionHandleImpl m_impl;

};


// Type-erased holder of ConditionWriteHandles
class AnyConditionWriteHandle
{
public:

   // Encapsulate a concrete condition write handle into the holder
   template< typename T >
   AnyConditionWriteHandle( ConditionWriteHandle<T> && handle );

   // Get read-only access to the stored handle. PrototypeBugDetected will be thrown if the type is wrong.
   template< typename T >
   const ConditionWriteHandle<T> & get() const;

   // Access the hasValue() method of the stored handle
   bool hasValue( const ConditionSlot & slot ) const;


private:

   AnyConditionHandleImpl m_impl;

};

}


// Inline implementations
#include "detail/AnyConditionHandle.ipp"

#endif
