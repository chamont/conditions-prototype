#include "ConditionStoreInternals.hpp"


namespace detail
{

VersionedCondition::VersionedCondition( const OptionalConditionKind & p_kind,
                                        const size_t                  p_capacity )
   : kind{ p_kind }
   , data( p_capacity )
{ }

}
