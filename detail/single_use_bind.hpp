#ifndef SINGLE_USE_BIND_HPP
#define SINGLE_USE_BIND_HPP

#include <functional>
#include <utility>

#include "detail/SingleUseBindWrapper.hpp"


namespace detail
{

// In C++11, which does not have move capture, how does one go about moving data in and out of a lambda?
// This is something we often want with future continuations, in order to iteratively construct a dataset:
// take a container in, add something to it, then push it out towards the next continuation.
//
// std::bind does half of what we want: it can receive and store the moved value, but it does not forward it
// to the inner callable, instead passing it by lvalue reference. So we can easily move data in, but not out.
//
// However, by injecting a wrapper layer between std::bind and the callable, we can move std::bind's inner
// data back into the callable and obtain the single use, move-in move-out future continuation that we seek.
//
template< typename F,
          typename... ArgsToBind >
auto single_use_bind( F&& f, ArgsToBind&&... bindArgs ) ->


// The return type of this function is an implementation detail, which we are forced to expose because the
// return type of std::bind is implementation-defined and C++11 does not have function return type deduction.
   decltype(
      std::bind(
         SingleUseBindWrapper< F, ArgsToBind... >{ std::forward<F>(f) },
         std::forward<ArgsToBind>(bindArgs)...
      )
   );

}


// Inline implementations
#include "detail/single_use_bind.ipp"

#endif
