#include "detail/DummyConditionProducer.hpp"


namespace detail
{

bool
DummyConditionProducer::outputMissing( const ConditionSlot & targetSlot ) const
{
   return m_impl->outputMissing( targetSlot );
}


void
DummyConditionProducer::produceOutput( const detail::TimePoint & eventTimestamp,
                                       const ConditionSlot     & targetSlot ) const
{
   m_impl->produceOutput( eventTimestamp, targetSlot );
}

}
