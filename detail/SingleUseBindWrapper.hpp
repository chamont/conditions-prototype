#ifndef SINGLE_USE_BIND_WRAPPER_HPP
#define SINGLE_USE_BIND_WRAPPER_HPP

#include <functional>
#include <type_traits>
#include <utility>


namespace detail
{

// Internally, single_use_bind puts this wrapper between the implementation of std::bind and the function
// that it calls, ensuring that bound parameters are moved in rather than merely passed by reference.
template< typename F,
          typename... ArgsToBind >
class SingleUseBindWrapper
{
private:

   // The original callable will be moved into the wrapper
   using FD = typename std::decay<F>::type;
   FD m_f;

   // To process the input from the std::bind implementation, we will need some metaprogramming hackery.
   // First of all, let us focus on what we want to achieve: "bound" inputs of std::bind, which are normally
   // irreversibly swallowed by its implementation, should be moved back into the callee instead.
   template< typename BoundArg,
             typename BoundArgD = typename std::decay<BoundArg>::type,
             typename EnableThisSubstitution = void >
   struct input_from_bind
   {
      static BoundArg && transform( BoundArg & arg ) { return std::move(arg); }
   };

   // We are happy with the way std::bind handles placeholders and other bind expressions, so we
   // perfect-forward the arguments associated with these inputs.
   template< typename PlaceholderArg, typename PlaceholderArgD >
   struct input_from_bind< PlaceholderArg, PlaceholderArgD,
      typename std::enable_if<
         std::is_placeholder<PlaceholderArgD>::value ||
         std::is_bind_expression<PlaceholderArgD>::value
      >::type
   > {
      template< typename T >
      static T && transform( T && arg ) { return std::forward<T>(arg); }
   };

   // We are also happy with the way std::bind handles reference wrappers, so we also forward the references.
   template< typename ReferenceWrapperArg, typename ReferencedType >
   struct input_from_bind< ReferenceWrapperArg, std::reference_wrapper<ReferencedType> >
   {
      static ReferencedType & transform( ReferencedType & arg ) { return arg; }
   };


public:

   // The wrapper is initialized by perfect-forwarding the original function into it
   explicit SingleUseBindWrapper( const FD & f );
   explicit SingleUseBindWrapper( FD && f );

   // When std::bind attempts to call the original function, we intercept the call and process its arguments
   // as above, using the original arguments passed to std::bind in order to guide us.
   template< typename... ArgsFromBindImpl >
   auto operator()( ArgsFromBindImpl &&... args ) ->

   // As before, don't look at this return type. It is an implementation detail forced upon us by C++11
      decltype( m_f( input_from_bind<ArgsToBind>::transform( std::forward<ArgsFromBindImpl>(args) )... ) );

};

}


// Inline implementations
#include "detail/SingleUseBindWrapper.ipp"

#endif
