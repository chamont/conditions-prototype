#ifndef SCHEDULER_HPP
#define SCHEDULER_HPP

#include <functional>
#include <vector>

#include "exceptions.hpp"

#include "detail/IConditionAlg.hpp"
#include "detail/timing.hpp"


// Forward declaration of other framework components
class ConditionSvc;


// The following class is a simple Gaudi scheduler mock that simulates event loops for testing and benchmarking
namespace detail
{

class Scheduler
{
public:

   // To allocate condition storage and carry out condition IO, the Scheduler needs access to the ConditionSvc
   Scheduler( ConditionSvc & conditionService );

   // To be scheduled, ConditionAlgs must register to the scheduler during initialization
   void registerConditionAlg( const IConditionAlg & alg );

   // Once provided with all of this data, the mock scheduler can carry out its event loop simulation.
   // If a scheduler stall is detected, it will be reported by throwing SchedulerStallDetected.
   class SchedulerStallDetected : public ConditionPrototypeException { };
   void simulateEventLoop( const std::vector< detail::TimePoint > & eventTimestamps );


private:

   ConditionSvc & m_conditionService;
   using ConditionAlgPtr = const IConditionAlg *;
   using ConditionAlgList = std::vector< ConditionAlgPtr >;
   ConditionAlgList m_conditionAlgs;

};

}

#endif
