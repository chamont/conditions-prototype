#ifndef ANY_CONDITION_HANDLE_IMPL_HPP
#define ANY_CONDITION_HANDLE_IMPL_HPP

#include <memory>
#include <type_traits>

#include "ConditionHandle.hpp"


namespace detail
{

// Type-erased holder of ConditionHandles, used to implement AnyConditionReadHandle and AnyConditionWriteHandle
class AnyConditionHandleImpl
{
public:
   
   // Access the hasValue method of the stored handle
   bool hasValue( const ConditionSlot & slot ) const;

   // Encapsulate a condition handle
   template<
      typename ConditionHandleType,
      typename Enable = typename std::enable_if<
         std::is_base_of< ConditionHandleBase, ConditionHandleType >::value
      >::type
   >
   AnyConditionHandleImpl( ConditionHandleType && handle );

   // Get read-only access to the stored handle (PrototypeBugDetected will be thrown if the type is wrong)
   template<
      typename ConditionHandleType,
      typename Enable = typename std::enable_if<
         std::is_base_of< ConditionHandleBase, ConditionHandleType >::value
      >::type
   >
   const ConditionHandleType & get() const;

   
private:

   // Because std::any requires a CopyConstructible value type, we cannot use it here
   std::unique_ptr< const ConditionHandleBase > m_holder;

};

}


// Inline implementations
#include "detail/AnyConditionHandleImpl.ipp"

#endif
