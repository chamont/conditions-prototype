template< typename T >
ConditionReadHandle<T>
TransientConditionStorageSvc::registerInput( const detail::ConditionUserID & client,
                                             const detail::ConditionID     & targetID )
{
   // Register our client as a consumer of this condition
   m_dataflow.registerConsumer<T>( client, targetID );
   
   // Produce a handle allowing the client to read this condition
   return ConditionReadHandle<T>{ m_storage.registerCondition( targetID, {} ) };
}


template< typename T >
ConditionWriteHandle<T>
TransientConditionStorageSvc::registerOutput( const detail::ConditionUserID & client,
                                              const detail::ConditionID     & targetID,
                                              const ConditionKind             targetKind )
{
   // Register our client as the producer of this condition
   m_dataflow.registerProducer<T>( client, targetID );
   
   // Produce a handle allowing the client to write this condition
   return ConditionWriteHandle<T>{ m_storage.registerCondition( targetID, targetKind ) };
}
