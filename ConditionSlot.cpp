#include "ConditionSlot.hpp"
#include "TransientConditionStorageSvc.hpp"


ConditionSlot::~ConditionSlot()
{
   if( m_isOwner ) {
      m_host.get().liberateSlot( *m_id );
   }
}
