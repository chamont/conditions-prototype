template< typename Result,
          typename... Args >
void
ConditionTransformer< Result(Args...) >::execute( const ConditionSlot & slot ) const
{
   executeImpl( slot,
                cpp_next::index_sequence_for<Args...>{} );
}


template< typename Result,
          typename... Args >
ConditionTransformer< Result(Args...) >::ConditionTransformer(       ConditionSvc        &  conditionService,
                                                                     detail::Scheduler   &  scheduler,
                                                               const detail::ConditionID &  resultID,
                                                                     ArgsIDs             && argsIDs )
   : ConditionAlgBase{ conditionService, scheduler }
   , resultHandle( ConditionAlgBase::registerOutput<Result>( resultID ) )
   , argsHandles{ registerArgs( std::move(argsIDs),
                                cpp_next::index_sequence_for<Args...>{} ) }
{ }


template< typename Result,
          typename... Args >
template< std::size_t... ArgsIndexes >
typename ConditionTransformer< Result(Args...) >::ArgsHandles
ConditionTransformer< Result(Args...) >::registerArgs( ArgsIDs && argsIDs,
                                                       cpp_next::index_sequence<ArgsIndexes...> )
{
   // Build the tuple of ConditionReadHandle references corresponding to the input arguments
   return ArgsHandles{ std::cref( ConditionAlgBase::registerInput<Args>( argsIDs[ArgsIndexes] ) )... };
}


template< typename Result,
          typename... Args >
template< std::size_t... ArgsIndexes >
void
ConditionTransformer< Result(Args...) >::executeImpl( const ConditionSlot & slot,
                                                      cpp_next::index_sequence<ArgsIndexes...> ) const
{
   // Fetch the arguments from the associated handles, derive the output, and put it in the result handle
   resultHandle.put(
      slot,
      deriveOutput(
         std::get<ArgsIndexes>( argsHandles ).get().get( slot )...
      )
   );
}


template< typename Result,
          typename... Args >
ConditionData< Result >
ConditionTransformer< Result(Args...) >::deriveOutput( const ConditionData<Args> & ... args ) const
{
   // The output is derived by applying the user-supplied functor to the arguments, and computing
   // the output IoV as the intersection of input IoVs.
   return ConditionData< Result >{
      (*this)( args.value... ),
      detail::TimeInterval::intersect( { args.iov... } )
   };
}
