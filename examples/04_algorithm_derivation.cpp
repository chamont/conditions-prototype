#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionKind.hpp"
#include "ConditionSlot.hpp"
#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/identifiers.hpp"
#include "detail/timing.hpp"

#include "examples/examples_shared.hpp"
#include "examples/StringConcatenator.hpp"


// This example showcases how ConditionAlgs can be used to automate condition derivation.
// Since it relies on ConditionTransformer, it requires algorithm-based IO to be disabled.
int main()
{
#ifndef ALLOW_IO_IN_ALGORITHMS

   // Parameters of this usage example are defined here
   const detail::ConditionID firstRawConditionID{ 1 };
   const detail::ConditionID secondRawConditionID{ 2 };
   const detail::ConditionID derivedConditionID{ 3 };
   const detail::ConditionUserID rawProducerID{ 1 };
   const detail::ConditionUserID derivedConsumerID{ 2 };
   
   // Set up the condition infrastructure for a storage capacity of one condition slot
   examples_shared::print( "Initializing the condition infrastructure..." );
   const std::size_t storageCapacity{ 1 };
   TransientConditionStorageSvc transientStore{ storageCapacity };
   ConditionSvc conditionService{ transientStore };
   detail::Scheduler scheduler{ conditionService };

   // Here, our condition derivation will be, as before, a string concatenation operation.
   // However, this time, it will be carried out using a ConditionAlg.
   examples_shared::print( "Setting up a condition derivation algorithm..." );
   StringConcatenator concatAlg{
      conditionService,
      scheduler,
      derivedConditionID,
      { firstRawConditionID, secondRawConditionID }
   };
   
   // We will still need a way to inject the raw conditions in, and to read the derived condition
   examples_shared::print( "Setting up condition injection and readout..." );
   const auto derivedReadHandle = transientStore.registerInput< std::string >(
      derivedConsumerID,
      derivedConditionID
   );
   const auto secondRawWriteHandle = transientStore.registerOutput< std::string >(
      rawProducerID,
      secondRawConditionID,
      ConditionKind::RAW
   );
   const auto firstRawWriteHandle = transientStore.registerOutput< std::string >(
      rawProducerID,
      firstRawConditionID,
      ConditionKind::RAW
   );
   
   // Report the framework's view of the condition dataflow
   const auto dataflowMetadata = transientStore.getConditionDataflow();
   examples_shared::printDataflowMetadata( dataflowMetadata );
   
   // Now, let us study the basic ConditionAlg operation
   {
      // Set up a condition slot for an event at timestamp 2
      examples_shared::newline();
      examples_shared::print( "Allocating storage for timestamp 2..." );
      ConditionSlotFuture firstSlotFuture = transientStore.allocateSlot( 2 );
      const ConditionSlot firstSlot = firstSlotFuture.get();

      // At this stage, our ConditionAlg cannot run yet because its inputs are missing
      examples_shared::check( !concatAlg.inputsPresent(firstSlot) );

      // Insert the raw condition data into the condition slot
      examples_shared::print( "Writing raw condition data..." );
      firstRawWriteHandle.put(
         firstSlot,
         { "Hello ", { 0, 3 } }
      );
      secondRawWriteHandle.put(
         firstSlot,
         { "world !", { 1, 5 } }
      );

      // Our ConditionAlg is now ready to run...
      examples_shared::check( concatAlg.inputsPresent( firstSlot ) );

      // ...and we should run it, because we need its output in order to proceed
      examples_shared::check( concatAlg.outputMissing( firstSlot ) );

      // So let's run it
      examples_shared::print( "Running the condition derivation algorithm..." );
      concatAlg.execute( firstSlot );

      // The derived condition data is now available for our viewing pleasure
      examples_shared::print( "Accessing the derived condition data..." );
      const ConditionData< std::string > & output = derivedReadHandle.get( firstSlot );
      examples_shared::printCondition( output );
   }

#else

   examples_shared::print( "This example is only available when algorithm IO is disabled. Sorry." );

#endif
  
   return 0;
}
