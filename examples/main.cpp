// DEBUG : Include every header here in order to trigger header compilation errors

#include "cpp_next/any.hpp"
#include "cpp_next/future.hpp"
#include "cpp_next/optional.hpp"
#include "cpp_next/utility.hpp"

#include "ConditionAlgBase.hpp"
#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionIOSvcBase.hpp"
#include "ConditionKind.hpp"
#include "ConditionSlot.hpp"
#include "ConditionSvc.hpp"
#include "ConditionTransformer.hpp"
#include "exceptions.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/AnyConditionData.hpp"
#include "detail/AnyConditionHandle.hpp"
#include "detail/AnyConditionHandleImpl.hpp"
#include "detail/ConditionDataflow.hpp"
#include "detail/ConditionDataSet.hpp"
#include "detail/ConditionHandleImpl.hpp"
#include "detail/ConditionSlotID.hpp"
#include "detail/ConditionSlotIDFutures.hpp"
#include "detail/ConditionSlotKnowledge.hpp"
#include "detail/ConditionStore.hpp"
#include "detail/ConditionStoreInternals.hpp"
#include "detail/ConditionUsage.hpp"
#include "detail/DummyConditionProducer.hpp"
#include "detail/dynamic_array.hpp"
#include "detail/IConditionAlg.hpp"
#include "detail/IConditionIOSvc.hpp"
#include "detail/identifiers.hpp"
#include "detail/Scheduler.hpp"
#include "detail/SharedConditionData.hpp"
#include "detail/single_use_bind.hpp"
#include "detail/SingleUseBindWrapper.hpp"
#include "detail/timing.hpp"

#include "examples/examples_shared.hpp"
#include "examples/DummyConditionIOSvc.hpp"
#include "examples/PassthroughAlg.hpp"
#include "examples/StringConcatenator.hpp"


int main()
{
   // DEBUG: Usage examples originate in main.cpp, and are subsequently moved to a dedicated
   //        code unit once they become feature-complete.
   
   return 0;
}
