#ifndef STRING_CONCATENATOR_HPP
#define STRING_CONCATENATOR_HPP

#include <string>

#include "ConditionTransformer.hpp"

#include "detail/identifiers.hpp"


// This example algorithm relies on ConditionTransformer, and is thus only available if Algorithm IO is disabled
#ifndef ALLOW_IO_IN_ALGORITHMS

// The algorithm takes two strings inputs, concatenates them, and returns that as an output
struct StringConcatenator : ConditionTransformer< std::string( std::string, std::string ) >
{

   // To initialize this algorithm, we need to access the ConditionSvc and know our input and output identifiers
   StringConcatenator(       ConditionSvc                  &  conditionService,
                             detail::Scheduler             &  scheduler,
                       const detail::ConditionID           &  resultID,
                             ConditionTransformer::ArgsIDs && argsIDs );

   // This is where the algorithm's core logic is implemented
   std::string operator()( const std::string & left,
                           const std::string & right ) const;

};

// Inline implementations
#include "examples/StringConcatenator.ipp"

#endif

#endif
