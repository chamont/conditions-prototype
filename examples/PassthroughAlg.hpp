#ifndef PASSTHROUGH_ALG_HPP
#define PASSTHROUGH_ALG_HPP

#include <string>

#include "ConditionTransformer.hpp"

#include "detail/identifiers.hpp"


// This example algorithm relies on ConditionTransformer, and is thus only available if Algorithm IO is disabled
#ifndef ALLOW_IO_IN_ALGORITHMS

// The algorithm simply feeds back its input as an output
template< typename T >
struct PassthroughAlg : ConditionTransformer< T( T ) >
{

   // To initialize this algorithm, we need to access the ConditionSvc and know our input and output identifiers
   PassthroughAlg(       ConditionSvc                  &  conditionService,
                         detail::Scheduler             &  scheduler,
                   const detail::ConditionID           &  resultID,
                   const detail::ConditionID           &  argID );

   // This is where the algorithm's core logic is implemented
   T operator()( const T & arg ) const;

};

// Inline implementations
#include "examples/PassthroughAlg.ipp"

#endif

#endif
