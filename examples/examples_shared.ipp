#include <iostream>

#include "exceptions.hpp"

#include "detail/ConditionUsage.hpp"


namespace examples_shared
{

inline
void
check( const bool predicate )
{
   if( !predicate ) fail();
}


inline
void
fail()
{
   throw PrototypeBugDetected{};
}


inline
void
print( const std::string & text )
{
   std::cout << "* " << text << std::endl;
}


inline
void
printDataflowMetadata( const detail::ConditionDataflow::Metadata & metadata )
{
   print( "Here is the framework's view of the condition dataflow:" );
   for( const auto & mapIterator : metadata )
   {
      const detail::ConditionUsage & usage = mapIterator.second;
      std::cout << "   - Condition " << mapIterator.first << " is produced by user ";
      std::cout << *usage.producer() << " and consumed by users { ";
      for( const auto & reader : usage.consumers() ) {
         std::cout << reader << ' ';
      }
      std::cout << '}' << std::endl;
   }
}


template< typename T >
void
printCondition( const ConditionData<T> & data )
{
   std::cout << "   - Value: " << data.value << std::endl;
   std::cout << "   - IoV: [" << data.iov.start() << ", " << data.iov.end() << ']' << std::endl;
}


inline
void
newline()
{
   std::cout << std::endl;
}


inline
void
Timer::start()
{
   m_start = Clock::now();
}


inline
Timer::Duration
Timer::stop() const
{
   const TimePoint end = Clock::now();
   const Duration elapsed = std::chrono::duration_cast<Duration>( end - m_start );
   return elapsed;
}

}
