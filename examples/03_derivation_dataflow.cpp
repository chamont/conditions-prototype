#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionKind.hpp"
#include "ConditionSlot.hpp"
#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/identifiers.hpp"
#include "detail/timing.hpp"

#include "examples/examples_shared.hpp"


// This example builds upon the "basicDataflow" one by presenting derived conditions
int main()
{
   // Parameters of this usage example are defined here
   const detail::ConditionID rawConditionID{ 1 };
   const detail::ConditionID derivedConditionID{ 2 };
   const detail::ConditionUserID rawProducerID{ 1 };
   const detail::ConditionUserID rawConsumerID{ 2 };
   const detail::ConditionUserID derivedProducerID{ rawConsumerID };
   const detail::ConditionUserID derivedConsumerID{ 3 };
   
   // Set up the condition infrastructure for a storage capacity of one condition slot
   examples_shared::print( "Initializing the condition infrastructure..." );
   const std::size_t storageCapacity{ 1 };
   TransientConditionStorageSvc transientStore{ storageCapacity };
   ConditionSvc conditionService{ transientStore };
   
   // Here, in addition to a raw condition, we also have another condition that is derived from it
   examples_shared::print( "Setting up condition dataflow..." );
   const auto rawReadHandle = transientStore.registerInput< std::string >(
      rawConsumerID,
      rawConditionID
   );
   const auto derivedReadHandle = transientStore.registerInput< std::string >(
      derivedConsumerID,
      derivedConditionID
   );
   const auto derivedWriteHandle = transientStore.registerOutput< std::string >(
      derivedProducerID,
      derivedConditionID,
      ConditionKind::DERIVED
   );
   const auto rawWriteHandle = transientStore.registerOutput< std::string >(
      rawProducerID,
      rawConditionID,
      ConditionKind::RAW
   );
   
   // Report the framework's view of the condition dataflow
   const auto dataflowMetadata = transientStore.getConditionDataflow();
   examples_shared::printDataflowMetadata( dataflowMetadata );
   
   // Condition slot allocation, filling and sharing have slightly different semantics for derived conditions
   {
      // Set up a condition slot for an event at timestamp 1
      examples_shared::newline();
      examples_shared::print( "Allocating storage for timestamp 1..." );
      ConditionSlotFuture firstSlotFuture = transientStore.allocateSlot( 1 );
      const ConditionSlot firstSlot = firstSlotFuture.get();

      // Insert some raw condition data into the condition slot
      examples_shared::print( "Writing raw condition data..." );
      rawWriteHandle.put(
         firstSlot,
         { "This is test data", { 0, 2 } }
      );
      
      // Try to set up another condition slot for an event at timestamp 0. Here, the request is delayed, but not
      // for the same reason as before. The problem here is that condition derivation should be carried out only
      // once, and it is the first event requesting a condition slot that is responsible for this work.
      examples_shared::print( "Requesting storage for timestamp 0..." );
      ConditionSlotFuture secondSlotFuture = transientStore.allocateSlot( 0 );
      examples_shared::check( !secondSlotFuture.is_ready() );

      // Now, let us carry out (simulated) condition derivation
      examples_shared::print( "Computing derived condition..." );
      const ConditionData< std::string > & rawCondition = rawReadHandle.get( firstSlot );
      derivedWriteHandle.put(
         firstSlot,
         { rawCondition.value + ", after derivation", rawCondition.iov }
      );

      // The condition slot is now fully constructed, and our second event can get access to it
      const ConditionSlot secondSlot = secondSlotFuture.get();
      examples_shared::check( secondSlot == firstSlot );

      // We can access the derived condition data just like we accessed the raw data before
      examples_shared::print( "Accessing the derived condition data..." );
      const ConditionData< std::string > & output = derivedReadHandle.get( firstSlot );
      examples_shared::printCondition( output );
   }
   
   // Lazy garbage collection also applies to derived conditions
   {
      // Set up a condition slot for an event at timestamp 2
      examples_shared::newline();
      examples_shared::print( "Allocating storage for timestamp 2..." );
      ConditionSlotFuture thirdSlotFuture = transientStore.allocateSlot( 2 );
      const ConditionSlot thirdSlot = thirdSlotFuture.get();

      // Here, the derived condition data has been preservied, as before
      examples_shared::print( "Accessing the cached condition data..." );
      examples_shared::check( derivedWriteHandle.hasValue( thirdSlot ) );
      examples_shared::printCondition( derivedReadHandle.get( thirdSlot ) );
   }

   // When raw conditions are discarded, derived conditions are discarded as well
   {
      // Set up a condition slot for a mismatching timestamp
      examples_shared::newline();
      examples_shared::print( "Allocating storage for timestamp 3..." );
      ConditionSlotFuture fourthSlotFuture = transientStore.allocateSlot( 3 );
      const ConditionSlot fourthSlot = fourthSlotFuture.get();

      // Both raw and derived conditions have been discarded
      examples_shared::check( !rawWriteHandle.hasValue( fourthSlot ) );
      examples_shared::check( !derivedWriteHandle.hasValue( fourthSlot ) );
   }
   
   return 0;
}
