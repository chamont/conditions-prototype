#ifndef DUMMY_CONDITION_IO_SVC_HPP
#define DUMMY_CONDITION_IO_SVC_HPP

#include <chrono>
#include <vector>

#include "cpp_next/future.hpp"

#include "ConditionIOSvcBase.hpp"
#include "ConditionSlot.hpp"

#include "detail/ConditionDataSet.hpp"
#include "detail/DummyConditionProducer.hpp"
#include "detail/identifiers.hpp"
#include "detail/timing.hpp"


// Forward declaration of other prototype components
class ConditionSvc;


// This dummy implementation of a condition IO service simulates I/O by loading data from a mock dataset with some
// arbitrary time delay. It can be used to test the latency hiding characteristics of the proposed infrastructure.
class DummyConditionIOSvc : public ConditionIOSvcBase
{
public:
   
   // Like any IO service, the dummy condition IO service must be told where the condition service is,
   // and which conditions it is expected to produce. In addition, for the purpose of IO simulation,
   // it should have an estimate of the response time of the IO device that it simulates.
   DummyConditionIOSvc(       ConditionSvc              &  conditionService,
                              detail::ConditionIDSet    && expectedProducts,
                        const std::chrono::milliseconds &  ioLatency );

   // The DummyConditionIOSvc does not carry out actual IO, but only simulates it. It should thus be
   // fed with a mock condition dataset to operate on.
   template< typename T >
   void setMockOutput( const detail::ConditionID         &  productID,
                             detail::ConditionDataSet<T> && output );

   // As expected by IConditionIOSvc, there is a way to start the condition IO simulation asynchronously
   cpp_next::future<void> startConditionIO( const detail::TimePoint & eventTimestamp,
                                            const ConditionSlot     & targetSlot ) final override;


private:
   
   // Internally, the IO of each individual condition is simulated by a DummyConditionProducer
   std::vector< detail::DummyConditionProducer > m_producers;
   
   // The simulated IO latency will be stored here
   const std::chrono::milliseconds m_ioLatency;

};


// Inline implementations
#include "DummyConditionIOSvc.ipp"

#endif
