#include <cstddef>

#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/ConditionDataflow.hpp"
#include "detail/ConditionDataSet.hpp"
#include "detail/identifiers.hpp"
#include "detail/Scheduler.hpp"
#include "detail/timing.hpp"

#include "examples/examples_shared.hpp"
#include "examples/DummyConditionIOSvc.hpp"


// This is a minimal example of an event loop simulation. In this example, conditions never change
// and are never read. This scenario can be used to estimate the intrinsic overhead of event scheduling.
int main()
{
   // Parameters of this usage example are defined here
   using ConditionType = std::string;
   using MockConditionDataSet = std::vector< ConditionData< ConditionType > >;
   const detail::ConditionID rawConditionID{ 1 };
   const detail::ConditionUserID exampleUserID{ 0 };
   const std::chrono::milliseconds ioLatency{ 0 };
   const std::size_t storageCapacity{ 1 };
   const std::size_t eventBatchSize{ 100000 };
   
   // The following objects will be used for benchmarking
   examples_shared::Timer timer;
   
   // Setup the condition infrastructure
   examples_shared::print( "Initializing the condition infrastructure..." );
   TransientConditionStorageSvc transientStore{ storageCapacity };
   ConditionSvc conditionService{ transientStore };
   detail::Scheduler scheduler{ conditionService };

   // Prepare an event batch
   examples_shared::print( "Setting up the event batch..." );
   std::vector< detail::TimePoint > eventBatch( eventBatchSize );
   std::iota( eventBatch.begin(), eventBatch.end(), 0 );

   // Add a simulation of condition IO
   examples_shared::print( "Setting up an IO simulation..." );
   DummyConditionIOSvc ioService{
      conditionService,
      { rawConditionID },
      ioLatency
   };
   ioService.setMockOutput< ConditionType >(
      rawConditionID,
      MockConditionDataSet{
         { "UNLIMITED VALIDITY!!!", detail::TimeInterval::INFINITE_INTERVAL }
      }
   );

   // Add a readout stage
   examples_shared::print( "Setting up condition readout..." );
   const auto rawReadHandle = transientStore.registerInput< ConditionType >(
      exampleUserID,
      rawConditionID
   );

   // Report the framework's view of the condition dataflow
   const auto dataflowMetadata = transientStore.getConditionDataflow();
   examples_shared::printDataflowMetadata( dataflowMetadata );
   
   // Run the event loop simulation
   examples_shared::print( "Running the simulated event loop..." );
   timer.start();
   scheduler.simulateEventLoop( eventBatch );
   const auto eventLoopDuration = timer.stop();

   // Display the simulation details
   const auto totalMiliseconds = eventLoopDuration.count();
   const int microsecondsPerEvent = float(totalMiliseconds) * 1000 / eventBatchSize;
   examples_shared::print( "Simulating "
                           + std::to_string(eventBatchSize) + " events took "
                           + std::to_string(totalMiliseconds) + " ms (~"
                           + std::to_string(microsecondsPerEvent) + " µs/event)" );
   
   return 0;
}
