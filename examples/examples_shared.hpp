#ifndef EXAMPLES_SHARED_HPP
#define EXAMPLES_SHARED_HPP

#include <chrono>
#include <string>

#include "ConditionData.hpp"

#include "detail/ConditionDataflow.hpp"


// This header factors out some commonalities between the example programs
namespace examples_shared
{

// === FREE FUNCTIONS ===

// This function plays a similar role to assert(). Its input should be guaranteed to be true by the prototype.
// If it is false, it indicates a bug, so we call fail() to abort the example.
void check( const bool predicate );

// This function should be called when an assertion has failed. It throws PrototypeBugDetected.
void fail();

// Print a line of example log with fancy formatting
void print( const std::string & text );

// Display the condition dataflow metadata of the prototype framework in a human-readable way
void printDataflowMetadata( const detail::ConditionDataflow::Metadata & metadata );

// Display the contents and IoV of a condition data blob
template< typename T >
void printCondition( const ConditionData<T> & data );

// Skip a line in the example output log
void newline();


// === SIMPLE PERFORMANCE TIMER ===

class Timer
{
public:

   // Durations will be reported in integer milliseconds
   using Duration = std::chrono::milliseconds;

   // Run this method to start the timer
   void start();

   // Run this method to stop the timer and measure elapsed time
   Duration stop() const;


private:

   using Clock = std::chrono::steady_clock;
   using TimePoint = Clock::time_point;
   TimePoint m_start;

};

}


// Inline implementations
#include "examples/examples_shared.ipp"

#endif
