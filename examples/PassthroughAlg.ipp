template< typename T >
PassthroughAlg<T>::PassthroughAlg(       ConditionSvc                  &  conditionService,
                                         detail::Scheduler             &  scheduler,
                                   const detail::ConditionID           &  resultID,
                                   const detail::ConditionID           &  argID )
   : ConditionTransformer<T(T)>( conditionService,
                                 scheduler,
                                 resultID,
                                 { argID } )
{ }


template< typename T >
T
PassthroughAlg<T>::operator()( const T & arg ) const
{
   return arg;
}
