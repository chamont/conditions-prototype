inline
StringConcatenator::StringConcatenator(       ConditionSvc                  &  conditionService,
                                              detail::Scheduler             &  scheduler,
                                        const detail::ConditionID           &  resultID,
                                              ConditionTransformer::ArgsIDs && argsIDs )
   : ConditionTransformer( conditionService,
                           scheduler,
                           resultID,
                           std::move(argsIDs) )
{ }


inline
std::string
StringConcatenator::operator()( const std::string & left,
                                const std::string & right ) const
{
   return left + right;
}
