#include <thread>

#include "detail/single_use_bind.hpp"


inline
DummyConditionIOSvc::DummyConditionIOSvc(        ConditionSvc              &  conditionService,
                                                 detail::ConditionIDSet    && expectedProducts,
                                           const std::chrono::milliseconds &  ioLatency )
   : ConditionIOSvcBase{ conditionService, std::move(expectedProducts) }
   , m_ioLatency{ ioLatency }
{ }


template< typename T >
void
DummyConditionIOSvc::setMockOutput( const detail::ConditionID         &  productID,
                                          detail::ConditionDataSet<T> && output )
{
   
   // Register a write handle to the proposed condition
   auto writeHandle = ConditionIOSvcBase::registerOutput<T>( productID );
   
   // Set up a DummyConditionProducer using this write handle and the user-specified condition dataset
   m_producers.emplace_back( std::move(writeHandle), std::move(output) );
}


inline
cpp_next::future<void>
DummyConditionIOSvc::startConditionIO( const detail::TimePoint & eventTimestamp,
                                       const ConditionSlot     & targetSlot )
{
   // Make sure that all condition producers have been registered
   if( !ConditionIOSvcBase::conditionOutputsRegistered() ) throw CannotProduceCondition{};

   // Find out which condition producers must be run (because the associated output is missing)
   using ScheduledProducerList = std::vector< std::reference_wrapper< const detail::DummyConditionProducer > >;
   ScheduledProducerList scheduledProducers;
   for( const auto & producer : m_producers )
   {
      if( producer.outputMissing( targetSlot ) ) {
         scheduledProducers.emplace_back( producer );
      }
   }

   // If all output conditions are ready, exit immediately
   if( scheduledProducers.empty() ) return cpp_next::make_ready_future();
   
   // Otherwise, we will simulate condition IO as follows
   auto ioSimulationImpl = [this, eventTimestamp]( ConditionSlot         && targetSlot,
                                                   ScheduledProducerList && scheduledProducers )
   {
      for( const auto & producerRef : scheduledProducers )
      {
         std::this_thread::sleep_for( m_ioLatency );
         producerRef.get().produceOutput( eventTimestamp, targetSlot );
      }
   };

   // The condition IO simulator above takes a non-owning reference to the condition slot.
   // In C++14, we'll do this directly in the lambda by using move capture and a mutable operator().
   auto ioSimulation = detail::single_use_bind( std::move(ioSimulationImpl),
                                                targetSlot.ref(),
                                                std::move(scheduledProducers) );
   
   // Run the simulated condition IO asynchronously
   return cpp_next::async( cpp_next::launch::async, std::move(ioSimulation) );
}
