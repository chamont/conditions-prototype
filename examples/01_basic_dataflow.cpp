#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionKind.hpp"
#include "ConditionSlot.hpp"
#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/identifiers.hpp"
#include "detail/timing.hpp"

#include "examples/examples_shared.hpp"


// This examples illustrates how condition handles and slots abstract away concrete condition storage
int main()
{
   // Parameters of this usage example are defined here
   const detail::ConditionID conditionID{ 1 };
   const detail::ConditionUserID producerID{ 1 };
   const detail::ConditionUserID consumerID{ 2 };
   
   // Set up the condition infrastructure for a storage capacity of one condition slot
   examples_shared::print( "Initializing the condition infrastructure..." );
   const std::size_t storageCapacity{ 1 };
   TransientConditionStorageSvc transientStore{ storageCapacity };
   ConditionSvc conditionService{ transientStore };
   
   // Set up a basic producer-consumer condition dataflow based around a string
   examples_shared::print( "Setting up condition dataflow..." );
   const auto readHandle = transientStore.registerInput< std::string >(
      consumerID,
      conditionID
   );
   const auto writeHandle = transientStore.registerOutput< std::string >(
      producerID,
      conditionID,
      ConditionKind::RAW
   );
   
   // Report the framework's view of the condition dataflow
   const auto dataflowMetadata = transientStore.getConditionDataflow();
   examples_shared::printDataflowMetadata( dataflowMetadata );
   
   // Now let's demonstrate condition slot allocation, filling and sharing
   {
      // Set up a condition slot for an event at timestamp 1
      examples_shared::newline();
      examples_shared::print( "Allocating storage for timestamp 1..." );
      ConditionSlotFuture firstSlotFuture = transientStore.allocateSlot( 1 );
      const ConditionSlot firstSlot = firstSlotFuture.get();

      // Try to set up another condition slot for an event at timestamp 0. This request will be delayed
      // until the condition infrastructure knows whether the slot for timestamp 1 can be reused or not
      examples_shared::print( "Requesting storage for timestamp 0..." );
      ConditionSlotFuture secondSlotFuture = transientStore.allocateSlot( 0 );
      examples_shared::check( !secondSlotFuture.is_ready() );

      // Now, insert some condition data into our condition slot
      examples_shared::print( "Writing condition data..." );
      writeHandle.put(
         firstSlot,
         { "This is test data", { 0, 3 } }
      );

      // Because this data is also valid for timestamp 0, the slot will be shared between timestamps 0 and 1
      const ConditionSlot secondSlot = secondSlotFuture.get();
      examples_shared::check( secondSlot == firstSlot );

      // We can access the condition data in the slot and read it without caring where exactly it was stored
      examples_shared::print( "Accessing the condition data..." );
      const ConditionData< std::string > & output = readHandle.get( firstSlot );
      examples_shared::printCondition( output );
   }
   
   // Condition slots are managed via RAII: they are automatically reclaimed once they get out of scope...
   examples_shared::print( "Liberated all storage slots" );
   examples_shared::check( transientStore.availableStorage() == storageCapacity );
   
   // ...but the associated condition data is garbage collected lazily, so as to catch further opportunities for reuse
   {
      // Set up a condition slot for an event at timestamp 2
      examples_shared::newline();
      examples_shared::print( "Allocating storage for timestamp 2..." );
      ConditionSlotFuture thirdSlotFuture = transientStore.allocateSlot( 2 );
      const ConditionSlot thirdSlot = thirdSlotFuture.get();

      // Because the previous condition is also valid for this event, it is reused by the framework
      examples_shared::print( "Accessing the cached condition data..." );
      examples_shared::check( writeHandle.hasValue( thirdSlot ) );
      examples_shared::printCondition( readHandle.get( thirdSlot ) );
   }

   // What happens when two condition storage allocations conflict, though?
   {
      ConditionSlotFuture fifthSlotFuture;
      {
         // Let's allocate one condition slot which can reuse the existing condition data, and one which cannot
         examples_shared::newline();
         examples_shared::print( "Allocating storage for timestamp 3..." );
         ConditionSlotFuture fourthSlotFuture = transientStore.allocateSlot( 3 );
         const ConditionSlot fourthSlot = fourthSlotFuture.get();
         
         // Try to setup another condition slot for an event at timestamp 4. Since we have only configured
         // our condition infrastructure for one condition storage slot, and the existing slot does not fit
         // this new request, it must wait until the former condition slot frees up.
         examples_shared::print( "Requesting storage for timestamp 4..." );
         fifthSlotFuture = transientStore.allocateSlot( 4 );
         examples_shared::check( !fifthSlotFuture.is_ready() );
      }

      // Once this is done, its slot request will be processed normally
      const ConditionSlot fifthSlot = fifthSlotFuture.get();

      // Since this event cannot reuse the existing condition data, said data will be flushed
      examples_shared::check( !writeHandle.hasValue( fifthSlot ) );
   }
   
   return 0;
}
