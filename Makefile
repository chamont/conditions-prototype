# === CONFIGURATION ===

# Toolchain definition
AR = ar
CXX = g++

# Build flags
DBGFLAGS := #-g -pg
OPTFLAGS := -O2
GCCFLAGS := $(DBGFLAGS) -pipe -pthread
CXXFLAGS := $(GCCFLAGS) $(OPTFLAGS) --std=c++11 -Wall -pedantic -march=native
ARFLAGS := rcs
EXTLIBS := -lboost_thread -lboost_system

# Target definition
EXAMPLES := $(wildcard examples/*.cpp)
EXECUTABLES := $(EXAMPLES:.cpp=.exe)
LIBNAME := ConditionsPrototype


# === ENVIRONMENT ===

# Headers
WORKDIR := $(shell pwd)
IFLAGS := -I$(WORKDIR)
HEADERS := $(wildcard *.hpp *.ipp \
                      cpp_next/*.hpp cpp_next/*.ipp \
                      detail/*.hpp detail/*.ipp \
                      examples/*.hpp examples/*.ipp)

# Object files
LIBSOURCES := $(wildcard *.cpp \
                         cpp_next/*.cpp \
                         detail/*.cpp)
LIBOBJECTS := $(LIBSOURCES:.cpp=.o)
SOURCES := $(LIBSOURCES) $(EXAMPLES)
OBJECTS := $(SOURCES:.cpp=.o)

# Libraries
LIBFILE := lib$(LIBNAME).a
LIBS := -l$(LIBNAME) $(EXTLIBS)
LFLAGS := $(GCCFLAGS) -L$(WORKDIR) $(LIBS)


# === RECIPES ===

all: $(EXECUTABLES) Makefile

%.exe: %.o $(LIBFILE) Makefile
	$(CXX) -static -o $@ $< $(LFLAGS)

$(LIBFILE): $(LIBOBJECTS) Makefile
	$(AR) $(ARFLAGS) $@ $(LIBOBJECTS)

%.o: %.cpp $(HEADERS) Makefile
	$(CXX) -o $@ $(CXXFLAGS) $(IFLAGS) -c $<

clean:
	rm -f $(OBJECTS)
	rm -f $(LIBFILE)
	rm -f $(EXECUTABLES)

